/*(1) Find the names of departments located in Sydney, Australia.
*/
SELECT * FROM DEPARTMENT WHERE city = 'Sydney' AND country_name = 'Australia';

/*country_name = ''
(2) Find the titles of jobs that offer salary in a range between 7500 and 8500
inclusive. Note, that a job with a salary range between 7600 and 8000 should be
included in the answer.
*/

SELECT job_title FROM EMPLOYEE WHERE salary >= 7500 AND salary <= 8500;

/*
(3) Find the full names of employees who are the topmost level supervisors, i.e. who are
not supervised by any other employee.
*/

SELECT first_name, last_name
FROM EMPLOYEE
WHERE supervisor_id IS NULL;

/*
(4) Find the employee ids and job titles of employees whose jobs ended in 1998*/
SELECT employee_id, job_title
FROM JOBHISTORY
WHERE end_date < '1998/01/01';

/*(5) List the full names of all departments and full names of employees working in each
department. The results should be displayed in the descending order of department
names and the full names of employees from the same department must be listed in
the ascending order of the last names.*/

SELECT department_name, first_name, last_name
FROM EMPLOYEE NATURAL JOIN DEPARTMENT
ORDER BY department_name DESC, last_name ASC;

