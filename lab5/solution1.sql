/*(1) Insert into the database information about an employee.
Harry Potter, employee id 300, phone number 515.123.8182, hired at 10
February 2010. His email is harrypotter@gmail.com. He has been hired
as a Programmer. His salary is 7000 and his commission percentage is 50%. He
works in the department of Information Technology and his supervisor id is
103.
yyyy-mm-dd
*/
INSERT INTO EMPLOYEE VALUES (300,'Harry','Potter','harrypotter@gmail.com','515.123.8182','2010-02-10','Programmer',7000.00,00.50,103,'Information Technology');

/*
(2) An employee with an employee id equal to 206 is not a supervisor of any other
employee. The employee decided to leave a company. Remove from a database all
information about the employee.
*/

DELETE FROM JOBHISTORY WHERE employee_id = '206';
DELETE FROM EMPLOYEE WHERE employee_id = '206';

/*
(3) A department Human Resources has been moved to a new location. The new
address is 100 Century Avenue, Shanghai, China. Post code is 200120 .
*/
INSERT INTO LOCATION VALUES( '100 Century Avenue','200120','Shanghai','','China');
UPDATE DEPARTMENT SET street_address = '100 Century Avenue', 
postal_code = '200120',
city = 'Shanghai',
country_name = 'China'
WHERE department_name = 'Human Resources';

