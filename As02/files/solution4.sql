/*(1) It should be possible to store information about the total number of positions needed
by each employer in a table EMPLOYER and about the total number of applications
submitted by each applicant in a table APPLICANT so far
*/
ALTER TABLE EMPLOYER ADD COLUMN positions_needed INT(5) NOT NULL;

ALTER TABLE APPLICANT ADD COLUMN total_applications INT(5) NOT NULL;

/*
(2) The following columns:
extra
specification
VARCHAR(50),
VARCHAR(2000) NOT NULL,
included in a relational table POSITION must be moved to another relational table
in order to decrease an average length of rows in the relational table POSITION.
Such modification may have a positive impact on the performance of query
processing on the table. A name of another relational table is up to you.
*/
ALTER TABLE POSITIONS DROP COLUMN extras;
ALTER TABLE POSITIONS DROP COLUMN specification;

CREATE TABLE POSITION_INFO (
	extra VARCHAR(50), 
	specification VARCHAR(2000) NOT NULL 
);

/*
(3) The relational tables SPOSSESSED and SNEEDED must have two foreign keys
each. Discover the foreign keys and add the missing foreign keys to both tables.
*/

ALTER TABLE SPOSSESSED ADD CONSTRAINT spossessed_fk FOREIGN KEY (sname) REFERENCES SKILL(sname);

ALTER TABLE SPOSSESSED ADD CONSTRAINT spossessed_fk2 FOREIGN KEY (anumber) REFERENCES APPLICANT(anumber);


ALTER TABLE SNEEDED ADD CONSTRAINT sneeded_fk FOREIGN KEY (sname) REFERENCES SKILL(sname);

ALTER TABLE SNEEDED ADD CONSTRAINT sneeded_fk2 FOREIGN KEY (pnumber) REFERENCES POSITIONS(pnumber);
/*
(4) Add the domain constraints that restrict the values in both columns slevel in the
relational tables SPOSSESSED and SNEEDED to the positive integer numbers in a
range from 1 to 10 inclusive.
*/
ALTER TABLE SNEEDED ADD CHECK (sname >= 1 AND sname <=10);
ALTER TABLE SPOSSESSED ADD CHECK (sname >= 1 AND sname <=10);
/*
(5) Information about gender of applicant is needed. The information fax of
employer is no longer needed in the database.*/

ALTER TABLE APPLICANT ADD COLUMN gender CHAR(5);

ALTER TABLE EMPLOYER DROP COLUMN fax;




