CREATE VIEW EMPJOBS(ENUM, NAME, EMAIL, FINISHEDJOBS ) AS
(	SELECT EMPLOYEE.employee_id,  concat(EMPLOYEE.first_name, " ", EMPLOYEE.last_name), EMPLOYEE.email, count(JOBHISTORY.end_date) as c
	FROM EMPLOYEE   
		INNER JOIN JOBHISTORY   
    		ON EMPLOYEE.employee_id = JOBHISTORY.employee_id 
		GROUP BY EMPLOYEE.employee_id
		HAVING EMPLOYEE.employee_id >= 100);

grant select on csit115.* to csit115 with grant option;
grant select on csit115.EMPJOBS to brb405_1;

grant select (ENUM, NAME, EMAIL) on csit115.EMPJOBS to brb405_2;

SELECT mysql.user.user, mysql.db.db, mysql.db.select_priv, mysql.columns_priv.Column_priv, mysql.tables_priv.Table_priv, mysql.tables_priv.table_name
	FROM mysql.user,mysql.db, mysql.columns_priv, mysql.tables_priv where mysql.user.user='brb405_2' OR mysql.user.user='brb405_1'
ORDER BY mysql.user.user ASC;



/* task two */

SET autocommit=0;


update EMPLOYEE 
set department_name = 'Shipping'
where employee_id = 177;


update EMPLOYEE 
set department_name = 'Executive'
where employee_id = 144;

/*all employees that have the same job title must belong to the same department
JOB TITLE | EMPLOYEE ID | DEPARTMENT NAME*/

SELECT job_title as 'JOB TITLE', employee_id as 'EMPLOYEE ID', department_name as 'DEPARTMENT NAME'
FROM EMPLOYEE
WHERE employee_id =
	(SELECT employee_id
	FROM EMPLOYEE 
	WHERE department_name = 'Executive'
 		and job_title != 'President' 
		and job_title != 'Administration Vice President')
OR employee_id = (SELECT employee_id
	FROM EMPLOYEE 
	WHERE department_name = 'Shipping'
		and job_title != 'Stock Clerk' 
		and job_title != 'Stock Manager' 
		and job_title != 'Shipping Clerk');


