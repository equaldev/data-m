
use csit115;

SET autocommit=0;

START TRANSACTION;

update EMPLOYEE 
set department_name = 'Shipping'
where employee_id = 177;


update EMPLOYEE 
set department_name = 'Executive'
where employee_id = 144;

SELECT job_title as 'JOB TITLE', employee_id as 'EMPLOYEE ID', department_name as 'DEPARTMENT NAME'
FROM EMPLOYEE
WHERE employee_id =
	(SELECT employee_id
	FROM EMPLOYEE 
	WHERE department_name = 'Executive'
 		and job_title != 'President' 
		and job_title != 'Administration Vice President')
OR employee_id = (SELECT employee_id
	FROM EMPLOYEE 
	WHERE department_name = 'Shipping'
		and job_title != 'Stock Clerk' 
		and job_title != 'Stock Manager' 
		and job_title != 'Shipping Clerk');

ROLLBACK;


SELECT job_title as 'JOB TITLE', employee_id as 'EMPLOYEE ID', department_name as 'DEPARTMENT NAME'
FROM EMPLOYEE
WHERE employee_id =
	(SELECT employee_id
	FROM EMPLOYEE 
	WHERE department_name = 'Executive'
 		and job_title != 'President' 
		and job_title != 'Administration Vice President')
OR employee_id = (SELECT employee_id
	FROM EMPLOYEE 
	WHERE department_name = 'Shipping'
		and job_title != 'Stock Clerk' 
		and job_title != 'Stock Manager' 
		and job_title != 'Shipping Clerk');

