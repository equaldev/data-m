tee solution4.rpt;

SET GLOBAL general_log='ON';

SET GLOBAL log_output='TABLE';

TRUNCATE TABLE mysql.general_log;

notee;

source dbchange4.sql;

tee solution4.rpt;

SET GLOBAL general_log='OFF';

select * from mysql.general_log where argument like '%CREATE %' or argument like '%ALTER %' or argument like '%DROP %';

select argument, count(argument) as 'Count' from mysql.general_log where argument like '%SELECT %' or argument like '%INSERT %' or argument like '%UPDATE %' or argument like '%DELETE %'
GROUP BY argument
ORDER BY count(argument) ASC;


