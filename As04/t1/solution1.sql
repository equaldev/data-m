create user brb405 identified by 'a';


grant select on brb405.EMPLOYEE to brb405 with grant option;

grant select on brb405.JOBHISTORY to brb405 with grant option;

grant create view on brb405.* to brb405 with grant option;

ALTER USER brb405 WITH MAX_QUERIES_PER_HOUR 100 MAX_CONNECTIONS_PER_HOUR 10 MAX_USER_CONNECTIONS 3;

ALTER USER brb405 ACCOUNT LOCK;

SELECT mysql.user.user, mysql.tables_priv.Table_priv,mysql.tables_priv.Table_name, mysql.db.Create_view_priv, mysql.user.max_questions, mysql.user.max_connections, mysql.user.max_user_connections, mysql.user.account_locked
FROM mysql.user, mysql.db, mysql.tables_priv
WHERE mysql.user.user='brb405' AND (mysql.tables_priv.Table_name = 'EMPLOYEE' OR mysql.tables_priv.Table_name = 'JOBHISTORY');

