
/*
list differences before*/


select * from EMPLOYEE where employee_id not in (
select e.employee_id
from EMPLOYEE as e
inner join brb405_EMP as b 
on (	
	e.employee_id = b.employee_id

   ));
/*
list differences after*/

select * from brb405_EMP where employee_id not in (
select e.employee_id
from EMPLOYEE as e
inner join brb405_EMP as b 
on (	
	e.employee_id = b.employee_id

   ));



/*
union of both*/

select * from EMPLOYEE where employee_id not in (
	select e.employee_id
	from EMPLOYEE as e
	inner join brb405_EMP as b 
	on (	
		e.employee_id = b.employee_id

	   )
) 
union
select * from brb405_EMP where employee_id not in (
	select employee_id from EMPLOYEE
);



