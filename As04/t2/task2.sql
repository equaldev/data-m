/*mysqldump csit115 EMPLOYEE --user root --password --verbose --lock_tables > brb405.bak

mysql csit115 -u root -pcsit115 <  brb405.bak
*/

/*
In brief, the
script must first list all added rows, then all deleted rows, and finally all changed rows in
a relational table EMPLOYEE. It is allowed to use more than one SELECT statement to
implement this task.*/


select * from EMPLOYEE where employee_id not in (
select e.employee_id
from EMPLOYEE as e
inner join brb405_EMP as b 
on (	
	e.employee_id = b.employee_id

   ));

select * from brb405_EMP where employee_id not in (
select e.employee_id
from EMPLOYEE as e
inner join brb405_EMP as b 
on (	
	e.employee_id = b.employee_id

   ));




select employee_id from EMPLOYEE where employee_id not in (
	select e.employee_id
	from EMPLOYEE as e
	inner join brb405_EMP as b 
	on (	
		e.employee_id = b.employee_id

	   )
) 
union
select employee_id from brb405_EMP where employee_id not in (
	select employee_id from EMPLOYEE
);



