# Syntax:
**SELECT, FROM, WHERE, IN, AND, ON, JOIN, UNION, HAVING, GROUP BY**

*e.g:*
```
SELECT code, name
FROM DEPARTMENT
WHERE total_staff_number > 30;


SELECT * 
FROM COURSE
WHERE offered_by IN 
    (SELECT name
    FROM DEPARTMENT
    WHERE chair = 'James Bond' 
);

//Subqueries:

SELECT *
FROM COURSE JOIN (SELECT name
                FROM DEPARTMENT
                WHERE chair = 'James Bond') JB
            ON COURSE.offered_by = JB.name;

```
# Projection Queries
**Projection queries select the entire columns from a relational table and do not have a WHERE clause.**

```
//Find all information about the departments:

SELECT code, name, total_staff_number, chair, budget
FROM DEPARTMENT;

//OR

SELECT * FROM DEPARTMENT;


//The keyword DISTINCT can be used to elimate duplicates from the results of a query.

SELECT DISTINCT credits
FROM COURSE;
```
#Row Functions
**A row function is called and is process one time for each row selected from a relational database.**
```
//Uppercase format
SELECT UPPER(name)
FROM DEPARTMENT;

//Find the first three characters from all course codes and full titles of all courses
SELECT SUBSTR(coursenum, 1, 3), title
FROM COURSE;

//Display name of departments and budgets modified by +10%
SELECT name, 1.1*IFNULL(budget, 0)
FROM DEPARTMENT;
```

#Group Functions
**A group function is called and is process one time for a group of rows.**

```
//Find total number of rows:
SELECT COUNT(*)
FROM course;

//Total number of staff members in all departments
SELECT SUM(total_staff_number)
FROM DEPARTMENTS;
//DISTNCT does not apply to this - all rows will be counted.

//Find an average budget per each department
SELECT AVG(IFNULL(budget, 0))
FROM DEPARTMENT;

```

# Special Queries
**DUAL consists of one row and one table. Must specify FROM for syntactical integrity.**
**DUAL can be changed to anything, but it must be set as one row.**

SQL as a calculator:
```
SELECT 30 * 90.30
FROM DUAL;
```
SQL as adiary:
```
SELECT DATE_ADD(SYSDATE(), INTERVAL 1 DAY)
FROM DUAL;
```
Add two months to the current date:
```
SELECT DATE_ADD(SYSDATE(), INTERVAL 2 MONTH)
FROM DUAL;
```
Days passed since 1 January 2001:
```
SELECT DATEDIFF(SYSDATE(), '2001-01-01')
FROM DUAL;
```
As a word processor:
*Who am I?*
```
SELECT CONCAT('I am ', CURRENT_USER())
FROM DUAL; 
```
Substring of 'Hello World' that starts from the first 'e':
```
SELECT SUBSTR('Hello World, INSTR('Hello World, e), LENGTH('Hello World'))
FROM DUAL;
```

#Queries with Simple Conditions
**Sample Database: http://www.uow.edu.au/~jrg/115/LECTURES/12SELECT-2/12Select-2.pdf**

Find titles of all 6 credit points courses:
```
SELECT title 
FROM COURSE
WHERE credits = 6;
```
Find titles of all 6 or 12 credit point courses:
```
SELECT title 
FROM COURSE
WHERE credits IN(6, 12);
```
Find titles and course numbers of all courses with the word 'Database':
```
SELECT title, cnum
FROM COURSE
WHERE UPPER(title) LIKE '%DATABASE%';
```

#Queries with Boolean Expressions

Find titles of all 6 credit points courses offered by the Department of Physics:
```
SELECT title 
FROM COURSE
WHERE (credits = 6) AND (offered_by = 'Physics');
```

Find the title of all 6 credit point courses that are not offered by the department of physics:
```
SELECT title 
FROM COURSE
WHERE not (offered_by = 'Physics') AND (credits = 6);
```

Find the titles of all 6 credit point courses that are not offered by the department of physics:
```
SELECT title 
FROM COURSE
WHERE (offered_by != 'Physics') AND (credits = 6);
```

# Set Algebra Queries
**UNION operation**

Find the credit points of courses offered by the department of physics or the department of mathematics and **do not list duplicated rows.**

```
SELECT DISTINCT credits
FROM COURSE
WHERE (offered_by = 'Physics' ) OR (offered_by = 'Mathematics')
```
Or a neater way:
```
SELECT credits
FROM COURSE
WHERE offered_by = 'Physics'
UNION
SELECT credits
FROM COURSE
WHERE offered_by = 'Mathematics';
```
**UNION automatically eliminates duplicate rows**

# Sorting
Find the numbers and titles of all 6 credit point subjects sorted in an ascending order of titles
```
SELECT cnum, title
FROM COURSE
WHERE credits = 6;
ORDER BY title ASC;

SELECT cnum, title
FROM COURSE
WHERE credits = 6;
ORDER BY 2 ASC;
//Where 2 is the index of the column.
```

Find the numbers and titles of all 6 credit point subjects sorted in an descendng order of titles
```
SELECT cnum, title
FROM COURSE
WHERE credits = 6;
ORDER BY title DESC;
```

Find the numbers, titles and credit of all subjects sorted in an ascending order by credits and for all subjects with the same credits sorted in descending order by titles
```
SELECT credits, title, cnum
FROM COURSE
ORDER BY title DESC, credits ASC;
```

# Queries about a lack of value (NULL)

Find the titles of all courses which are not offered now:
```
SELECT title
FROM course
WHERE offered_by IS NULL;
```
*IS NULL is important, as NULL will not work.*

Find the titles of all courses which are now:
```
SELECT title
FROM course
WHERE offered_by IS NOT NULL;
```
#Grouping
Find the names of all departments that offer at least one course, but do not list the same names of departments:
```
SELECT DISTINCT offered_by
FROM COURSE;

SELECT offered_by
FROM COURSE
GROUP BY offered_by;
```
**GROUP BY** sorts a relational table over the attributes included in the **GROUP BY** list and creates the groups of rows with identical values of the attributes in **GROUP BY** list.
In the example above, the table **COURSE** is sorted with the values of the attribute offered_by.
The groups of rows with the identical value of 'offered_by' are created.

Grouping allows for application of group functions to each group created by the **GROUP BY** clause.

**Other examples:**
For each value of credit points, find the total number of courses have the specified credits
```
SELECT credits, count(*)
FROM COURSE
GROUP BY credits;
```

For each department, find the total number of credit points offered:
```
SELECT offered_by, sum(credits)
FROM COURSE
GROUP BY offered_by;
```

Find the largest number of courses offered by any department:
```
SELECT max(total)
FROM (SELECT count(*) total
    FROM COURSE
    GROUP BY offered_by) T;
```

# Grouping with Selections

Find the name of all departments that offer more than one course:
```
SELECT offered_by, count(*)
FROM COURSE
GROUP BY offered_by;
```

Find the names of all departments that offer more than 1 course:
```
SELECT offered_by, count(*)
FROM COURSE
GROUP BY offered_by
HAVING count(*) > 1;
```

# Join queries
**Sample database for the following:**
```
CREATE TABLE DEPARTMENT(
 name               VARCHAR(50)        NOT NULL,
 code               CHAR(5)            NOT NULL,
 total_staff_number DECIMAL(2)         NOT NULL,
 chair              VARCHAR(50)            NULL,
 budget             DECIMAL(9,1)       NOT NULL,
  CONSTRAINT dept_pkey PRIMARY KEY(name),
  CONSTRAINT dept_ckey1 UNIQUE(code),
  CONSTRAINT dept_ckey2 UNIQUE(chair),
  CONSTRAINT dept_check1 CHECK (total_staff_number BETWEEN 1 AND 50) );

  CREATE TABLE COURSE(
 cnum               CHAR(7)           NOT NULL,
 title              VARCHAR(200)      NOT NULL,
 credits            DECIMAL(2)        NOT NULL,
 offered_by         VARCHAR(50)           NULL,
  CONSTRAINT course_pkey PRIMARY KEY(cnum),
  CONSTRAINT course_check1 CHECK (credits IN (6, 12)),
  CONSTRAINT course_fkey1 FOREIGN KEY(offered_by)
                        REFERENCES DEPARTMENT(name) ON DELETE CASCADE );
```
**Find the titles of all courses offered by a department chaired by Peter**
*To implement the query we must use two tables: DEPARTMENT and COURSE*
*The rows from a table DEPARTMENT must be joined with (connected to) the respective rows in a relational table COURSE over a condition*

```
SELECT COURSE.title
FROM COURSE JOIN DEPARTMENT
            ON DEPARTMENT.name = COURSE.offered_by
WHERE DEPARTMENT.chair = 'Peter';
```

**Variations**

```
SELECT title
FROM COURSE JOIN DEPARTMENT
            ON name = offered_by
WHERE chair = 'Peter';

SELECT C.title
FROM COURSE C JOIN DEPARTMENT D
            ON D.name = C.offered_by
WHERE D.chair = 'Peter';

SELECT COURSE.title
FROM COURSE, DEPARTMENT
WHERE DEPARTMENT.name = COURSE.offered_by AND DEPARTMENT.chair = 'Peter';

SELECT title
FROM COURSE, DEPARTMENT
WHERE name = offered_by AND chair = 'Peter';
```
# ANSI SQL Syntax
The following implementations of the query **Find the titles of all courses offered by a department chaired by Peter** are consistent with ANSI SQL standard:
```
SELECT COURSE.title
FROM COURSE JOIN DEPARTMENT
            ON DEPARTMENT.name = COURSE.offered_by
WHERE DEPARTMENT.chair = 'Peter';

SELECT title
FROM COURSE JOIN DEPARTMENT
            ON name = offered_by
WHERE chair = 'Peter';

SELECT C.title
FROM COURSE C JOIN DEPARTMENT D
            ON D.name = C.offered_by
WHERE D.chair = 'Peter';
```
# Natural join queries
Consider a query: **Find the names of all employees from a department chaired by James Bond**
*A natural join query*
```
SELECT ename
FROM EMPLOYEE NATURAL JOIN DEPARTMENT
WHERE chair = 'James Bond';
```
*is equivalent to a join query:*
```
SELECT ename
FROM EMPLOYEE JOIN DEPARTMENT
              ON EMPLOYEE.dname = DEPARTMENT.dname
WHERE chair = 'James Bond';
```
# Column name join queries

*A column name join query*
```
SELECT ename
FROM EMPLOYEE JOIN DEPARTMENT
              USING(dname)
WHERE chair = 'James Bond';
```
*is equivalent to a join query*
```
SELECT ename
FROM EMPLOYEE JOIN DEPARTMENT
              ON  EMPLOYEE.dname = DEPARTMENT.dname
WHERE chair = 'James Bond';
```
# Cross join query
Consider a query: 
**Find all pairs of the names of employees and the names of chair people**
*A cross join join query:*
```
SELECT ename, chair
FROM EMPLOYEE CROSS JOIN DEPARTMENT;
```
*is equivalent to the following join queries:*
```
SELECT ename, chair
FROM EMPLOYEE JOIN DEPARTMENT;

SELECT ename, chair
FROM EMPLOYEE, DEPARTMENT;
```

# Join queries over more than 2 tables
**Consider the relational tables with the following schemas**
```
COURSE(cnum | title | credits)
STUDENT(snum | name | degree)
ENROLMENT(cnum | snum | edate | result)
```

A query find the names of all students who enrolled Java course can be implemented as the following join query:
```
SELECT STUDENT.name
FROM COURSE JOIN ENROLMENT
            ON COURSE.cnum = ENROLMENT.cnum
            JOIN STUDENT
            ON ENROLMENT.snum = STUDENT.snum
WHERE COURSE.title = 'Java';
```

**Variation for above**
```
SELECT STUDENT.name
FROM COURSE JOIN ENROLMENT
            ON COURSE.cnum = ENROLMENT.cnum
            JOIN STUDENT
            ON ENROLMENT.snum = STUDENT.snum
WHERE COURSE.title = 'Java';

SELECT STUDENT.name
FROM COURSE, ENROLMENT, STUDENT
WHERE COURSE.cnum = ENROLMENT.cnum AND ENROLMENT.snum = STUDENT.snum AND 
       COURSE.title = 'Java';

SELECT STUDENT.name
FROM ( SELECT *
       FROM COURSE JOIN ENROLMENT
                   ON COURSE.cnum = ENROLMENT.cnum 
       WHERE COURSE.title = 'Java' ) CE JOIN STUDENT
                                        ON CE.snum = STUDENT.snum
WHERE COURSE.title = 'Java';
```
# Self-join queries
*Consider a relational table with the following schema and contents*
```
+------+-------+---------+
| enum | name  | manager |
+------+-------+---------+
|   10 | John  |    NULL |
|   20 | Peter |      10 |
|   30 | Mary  |      10 |
|   40 | Mike  |      20 |
|   50 | Kate  |      20 |
|   60 | Greg  |      50 |
|   70 | Phil  |      50 |
+------+-------+---------+
```
# Planning
Consider a query to find the name of a manager of employee number 40.
We can "plan" the implementation in the following way:
 - (1) Find an employee number of manager of employee number 40
 - (2) Find a name of employee found in the previous query
```
SELECT manager 
FROM EMPLOYEE
WHERE enum = 40;

//Values Returned: 20

SELECT name 
FROM EMPLOYEE
WHERE enum = 20;

//Values Returned: Peter
```
This can be done in one SELECT statement instead of two, and there is more than one way to do it.
**Solution 1**
Assume that we have two identical relational tables E1 and E2
```
E1                                     E2
+------+-------+---------+             +------+-------+---------+   
| enum | name  | manager |             | enum | name  | manager |
+------+-------+---------+             +------+-------+---------+
|   10 | John  |    NULL |             |   10 | John  |    NULL |
|   20 | Peter |      10 |    -------->|   20 | Peter |      10 |
|   30 | Mary  |      10 |    |        |   30 | Mary  |      10 |
|   40 | Mike  |      20 |-----        |   40 | Mike  |      20 |
|   50 | Kate  |      20 |             |   50 | Kate  |      20 |
|   60 | Greg  |      50 |             |   60 | Greg  |      50 |
|   70 | Phil  |      50 |             |   70 | Phil  |      50 |
+------+-------+---------+             +------+-------+---------+
//  To find a name of manager of employee number 40 we take a row:
//
//  |   40 | Mike  |      20 |
// from table E1 
//  and then we join with a condition E1.manager = E2.enum with a row
//  |   20 | Peter |      10 |
// from a table E2. 
//  Query:

SELECT E2.name
FROM EMPLOYEE E1 JOIN EMPLOYEE E2
                 ON E1.manager = E2.enum
WHERE E1.enum = 40;

```
**Solution 2**
The second way to do it is to use inline views to combine the following queries:
```
SELECT manager 
FROM EMPLOYEE
WHERE enum = 40;

SELECT name 
FROM EMPLOYEE
WHERE enum = 20;
```
We use the first SELECT statement to create an inline view E40:
```
( SELECT manager 
FROM EMPLOYEE
WHERE enum = 40 ) E40
```
Then we join the inline view E40 with the table EMPLOYEE:
```
SELECT EMPLOYEE.name
FROM EMPLOYEE JOIN ( SELECT manager 
                     FROM EMPLOYEE
                     WHERE enum = 40 ) E40
              ON EMPLOYEE.enum = E40.manager;
```

In another query we find the names of all employees directly managed by Kate
```
+------+-------+---------+
| enum | name  | manager |
+------+-------+---------+
|   10 | John  |    NULL |
|   20 | Peter |      10 |
|   30 | Mary  |      10 |
|   40 | Mike  |      20 |
|   50 | Kate  |      20 |
|   60 | Greg  |      50 |
|   70 | Phil  |      50 |
+------+-------+---------+
```
We implement this in the following way:
 - (1) Find an employee number of an employee Kate
 - (2) Find the names of employees who have a number found in the previous query in a column manager

**Solution 1**
Assume that we have two identical relational tables E1 and E2
```
E1                                     E2
+------+-------+---------+             +------+-------+---------+   
| enum | name  | manager |             | enum | name  | manager |
+------+-------+---------+             +------+-------+---------+
|   10 | John  |    NULL |             |   10 | John  |    NULL |
|   20 | Peter |      10 |             |   20 | Peter |      10 |
|   30 | Mary  |      10 |             |   30 | Mary  |      10 |
|   40 | Mike  |      20 |             |   40 | Mike  |      20 |
|   50 | Kate  |      20 |    -------- |   50 | Kate  |      20 |
|   60 | Greg  |      50 |<---|        |   60 | Greg  |      50 |
|   70 | Phil  |      50 |<---|        |   70 | Phil  |      50 |
+------+-------+---------+             +------+-------+---------+

To find a number of employee Kate we take a row

|   50 | Kate  |      20 |

from a table E2 and we join it over a condition E2.enum = E1.manager with the rows

|   60 | Greg  |      50 |
|   70 | Phil  |      50 |

from a table E1
```
**Implementation**
```
SELECT E1.name
FROM EMPLOYEE E1 JOIN EMPLOYEE E2
                 ON E1.manager = E2.enum
WHERE E2.name = 'Kate';

```

**Solution 2**

*We use inline views to combine the following queries*

```
SELECT enum 
FROM EMPLOYEE
WHERE name = 'Kate';

SELECT name 
FROM EMPLOYEE
WHERE manager = 50;
```


We use the first SELECT statement to create an inline view KATE
```
( SELECT enum 
  FROM EMPLOYEE
  WHERE name = 'Kate' ) KATE
Then we join an inline view KATE with a relational table EMPLOYEE
SELECT EMPLOYEE.name
FROM EMPLOYEE JOIN ( SELECT enum 
                     FROM EMPLOYEE
                     WHERE name = 'Kate' ) KATE
              ON EMPLOYEE.manager = KATE.enum;
```


# Outer Join Queries
JOIN operation eliminates from both arguments the rows
 that cannot be joined with any row from the other argument.
```
 SELECT name, title
 FROM DPARTMENT JOIN COURSE
    ON name = offered_by;
```

#Left Outer Join Queries
Consider the query:
*Find the names of departments together with the titles of all courses offered by each department and include the names of departments that offer no courses at all.
```
SELECT name, title
 FROM DPARTMENT LEFT OUTER JOIN COURSE
    ON name = offered_by;
```
The operation LEFT OUTER JOIN includes into the results all rows from the left argument of the oepration.

# Right Outer Join
ref= 14-select4.pdf

#Nested Queries
A class of nested queries is based on a concept of inline views
Inline views can be used to reduce the complexity of query implementation
An inner query is created first and its outcomes are used as a relational table (inline view) in an outer query
For example, a query find the titles of courses offered by a department chaired by Peter is decomposed into the following two queries:

- **Q1**: Find a department chaired by Peter
- **Q2**: Find the titles of courses offered by a department found in **Q1**

A query Q1 is implemented first and then it is used in WHERE clause of query Q2
In nested queries SELECT statements are nested to theoretically unlimited level in WHERE clause
```
SELECT title
FROM COURSE
WHERE offered_by IN ( SELECT name
                      FROM DEPARTMENT
                      WHERE chair = 'Peter' );
```
If inner query returns more than one row the we must use IN istead of =.
A way how we implement a query find the titles of courses offered by a department chaired by Peter is the following
Create a inner query as inline view Q
```
( SELECT name
FROM DEPARTMENT
WHERE chair = 'Peter' ) Q
```
Create outer query that references an inline view Q created earlier
```
SELECT title
FROM COURSE
WHERE offered_by IN Q.name
```
Replace a reference Q to an inline view with the inline view
```
SELECT title
FROM COURSE
WHERE offered_by IN ( SELECT name
                      FROM DEPARTMENT
                      WHERE chair = 'Peter' );
```
**More Examples**
Another example, find the chairs of all departments that offer 12 credit point courses
An inner query as inline view Q
```
( SELECT offered_by
FROM COURSE
WHERE credits = 12) Q;
```
Create outer query that references an inline view Q created earlier
```
SELECT chair
FROM DEPARTMENT
WHERE name IN Q.offered_by
```
Replace a reference to an inline view Q with the inline view itself
```
SELECT chair
FROM DEPARTMENT
WHERE name IN ( SELECT offered_by
                FROM COURSE
                WHERE credits = 12 );
```
A query find the chairs of all departments that offer no courses is an example of **ANTIJOIN** operation
It is equivalent to a query find all rows in a relational table DEPARTMENT that cannot be joined with any row in a relational table COURSE
An inner query as inline view finds all departments that offer at least one course
```
( SELECT offered_by
FROM COURSE ) Q;
```
An outer query finds all chairs of deparments that are NOT included in the results of an inner query
```
SELECT chair
FROM DEPARTMENT
WHERE name NOT IN Q.offered_by
```
Finally, we replace a reference to an inline view Q with the inline view itself
```
SELECT chair
FROM DEPARTMENT
WHERE name NOT IN ( SELECT offered_by
                    FROM COURSE );
```
# Correlated nested queries
An inline view can reference the names of relational tables used in SELECT statement outer to the inline view
For example, consider a query: Find the chairs of departments that offer 12 credit point courses
Such query can be re-phrased as an equivalent query find the chairs of departments such that there exists at least one course worth 12 credit points offered by a department we are looking for
An inner query ... course worth 12 credit points offered by a department we are looking for is implemented as the following inline view
```
( SELECT *
FROM COURSE
WHERE credits = 12 AND offered_by = DEPARTMENT.name ) Q
```
An outer query find the chairs of departments such that there exists at least one course found in the inner quer is implemented in the folowing way
```
SELECT chair
FROM DEPARTMENT
WHERE EXISTS Q;
```
Finally, we replace a reference Q to an inline view with the inline view itself
```
SELECT chair
FROM DEPARTMENT
WHERE EXISTS ( SELECT *
               FROM COURSE
               WHERE credits = 12 AND offered_by = DEPARTMENT.name );
```
Another example where inline view references a name of relational table used in SELECT statement outer to the inline view is the following
Find the chairs of all departments that offer no courses
It is equivalent to a query find the chairs of departments such that does not exist a course offered by a department we are looking for
An inner query finds ... a course offered by a department we are looking for
```
( SELECT *
FROM COURSE
WHERE offered_by = DEPARTMENT.name ) Q;
```
An outer query finds all chairs of deparments such that does not exist a course found by an inner query
```
SELECT chair
FROM DEPARTMENT
WHERE NOT EXISTS Q;
```
Finally, we replace a reference to an inline view Q with the inline view itself
```
SELECT chair
FROM DEPARTMENT
WHERE NOT EXISTS( SELECT *
                  FROM COURSE
                  WHERE offered_by = DEPARTMENT.name );
```


# Relational Views

A relational view is a **virtual relational table** (derived relational table) that occupies no presistent storage and it is computed from very beginning each time it is used in SELECT statement.

A relational view is stored by a database management system as a pair (name of a view, SELECT statement that defines the structure and contents of the view)
Each time a name of relational view is used in SELECT statement its definition replaces the name of view and it becomes an inline view.

For example, create a relational view that contains the names of all departments together with the total number of courses offered by each department.
```
CREATE VIEW VDEPT( name, total_courses ) AS
( SELECT name, count(cnum)
  FROM DEPARTMENT LEFT OUTER JOIN COURSE
                  ON DEPARTMENT.name = COURSE.offered_by
                  GROUP BY name );
```
*Then a view VDEPT can be used to implement a query find the names of departments that offer more than 1 course:*

```
SELECT name
FROM VDEPT
WHERE total_courses > 1;

//The same query implemented with GROUP BY and HAVING clauses is the following

SELECT name, count(cnum)
FROM DEPARTMENT LEFT OUTER JOIN COURSE
                  ON DEPARTMENT.name = COURSE.offered_by
GROUP BY name
HAVING count(cnum) > 1;
```

A relational view can be used to reduce the complexity of SELECT statements
For example, a query find the chairs of departments that offer both 6 and 12 credit point courses is decomposed into the following queries:

- **V6**: Find the names of departments that offer 6 credit point courses
- **V12**: Find the names of departments that offer 12 credit point courses
- **VNAME**: Find the names of departments included in both V6 and V12
- **VCHAIR**: Find the chairs of departments included in VNAME

*The views are implemented in the following way:*

V6: Find the names of departments that offer 6 credit point courses
```
CREATE VIEW V6( name ) AS
( SELECT offered_by
  FROM COURSE
  WHERE credits = 6 );
```

V12: Find the names of departments that offer 12 credit point courses
```
CREATE VIEW V12( name ) AS
( SELECT offered_by
  FROM COURSE
  WHERE credits = 12 );
```

VNAME: Find the names of departments included in both V6 and V12
```
CREATE VIEW VNAME( name ) AS
( SELECT V6.name
  FROM V6 JOIN V12
       ON V6.name = V12.name );
```

VCHAIR: Find the chairs of departments included in VNAME
```
CREATE VIEW VCHAIR( chair ) AS
( SELECT DEPARTMENT.chair
  FROM VNAME JOIN DEPARTMENT
	     ON VNAME.name = DEPARTMENT.NAME );
```

**The final query is:**
```
SELECT *
FROM VCHAIR;
```

# Advanced DML statements

*Advanced Data Manipulation statements use subqueries implemented as SELECT statements inside DML statements*

For example, delete all courses offered by a department chaired by Peter
```
DELETE FROM COURSE
WHERE offered_by = ( SELECT name
                     FROM DEPARTMENT
                     WHERE chair = 'Peter' );
```
**Delete all departments that offer no courses:**
```
DELETE FROM DEPARTMENT
WHERE NOT EXISTS ( SELECT '1'
                   FROM COURSE
                   WHERE COURSE.offered_by = DEPARTMENT.name );
```
**Increase the total number of staff members by 5 in all departments that offer more than 20 courses:**
```
UPDATE DEPARTMENT
SET total_staff_number = total_staff_number + 5
WHERE name IN ( SELECT offered_by
                FROM COURSE
                GROUP BY offered_by
                HAVING COUNT(cnum) > 20 );

```
**Add to table DEPARTMENT a column that contains the total number of courses offered by each department and insert the correct values into the column:**
```
ALTER TABLE DEPARTMENT ADD ( total_courses DECIMAL(2) );
UPDATE DEPARTMENT
SET total_courses = ( SELECT COUNT(title)
                      FROM COURSE
                      WHERE COURSE.offered_by = DEPARTMENT.name );
```

Create a table that contains the names of departments together with the total number of courses offered by each department and insert correct data into the table
```
CREATE TABLE DCNT AS
( SELECT name, COUNT(cnum) TOTC
  FROM DEPARTMENT LEFT OUTER JOIN COURSE
                  ON DEPARTMENT.name = COURSE.offered_by
  GROUP BY name );
```
*Note, that NONE of the consistency constraints except NULL/NOT NULL constraints are copied from the relational tables DEPARTMENT and COURSE into a relational table DCN*

**To preserve the consistency constraints we create a relational table DCNT first!**
```
CREATE TABLE DCNT(
 name          VARCHAR(50)  NOT NULL,
 total_courses DECIMAL(2)    NOT NULL,
  CONSTRAINT DCNT_pkey PRIMARY KEY(name),
	     CONSTRAINT DCNT_fkey FOREIGN KEY (name) REFERENCES DEPARTMENT(name) );
```

*... and we load data into the table next:*
```
INSERT INTO DCNT
( SELECT name, COUNT(cnum)
  FROM DEPARTMENT LEFT OUTER JOIN COURSE
                  ON DEPARTMENT.name = COURSE.offered_by
  GROUP BY name );
```
