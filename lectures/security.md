#Data Vulnerability

**Coding Defects**

- Software defects are accidentally or intentionally built into the code during software development and include design flaws and coding mistakes (35% of successful attacks exploit these types of errors)
- Design flaws involve design decision that create an inherently insecure system
- Coding errors include both ordinary software bugs as well as features that were put in not by design but through oversight (and as a result of developers not thinking of all potential consequences)
- Coding errors include buffer overflows, race conditions, back doors into systems, and even nonrandom random-number generators


**Configuration errors**

- Configuration errors account for 65% of vulnerabilities
- Configuration errors include set up of unnecessary and dangerous services when a system is configured such that it brings up services and allows for connections that are not required
- It is usually caused by installation of a system with a default configuration rather than with precisely defined configuration that eliminates all features that are not required (it is easier to use default configuration because vendors prefer to offer an all-enabling starting configuration)

**Access administration errors**

- When access control includes configuration errors, entire security model falls apart
- Because most complex systems have elaborate access control schemes based on the concepts of groups, roles, permissions, delegation, etc it is easy to get the errors in access control configuration
- It is very hard to detect the cases that exploit such errors because it cannot be detected by intrusion detection or other monitoring systems due to incorrect assumptions that outside access looks correct

**SQL Injection**

SQL injection is a technique that exploits the applications using relational databases as their back end.
The technique uses the fact that applications have an available connection to a database and that the application composes SQL statements and send them to a database server to extract data or to perform certain functions.
SQL injection uses a fact that many of these applications compose such SQL statements by doing string concatenation of the fixed part of SQL statements along with user supplied data that forms WHERE clause or additional subqueries.
The technique is based on intentionally malformed user-supplied data that transform SQL statement from an innocent form into a malicious call that causes unauthorized access, deletion of data, or even theft of information
In all cases SQL injection as a technique is based on using bugs and vulnerabilities in an application.

**Injection Example**
The application receives USER ID and PASSWORD and it authenticates by checking USER ID and PASSWORD in USER table
Additionally the application does not validate what a user typed into these two fields and SQL statement is created by string concatenation.

The following piece of code implements the authentication

```
sqlString = "SELECT USERID FROM USER WHERE USERID = ' " &
userID& " ' AND PWD = ' " &pwd& " ' ";
result = GetQuery Result(sqlString);
if (result = "") then
	userHasBeenAuthenticated = False
else
	userHasBeenAutheticated = True
end if;
```

What happens when a user intentionally types in a malicious code like
```
USER ID: ' OR ' ' = '
PASSWORD: ' OR ' = '
```

In such a case *sqlString* variable obtains the following value
```
SELECT USERID FROM USER WHERE USERID = '' OR '' = '' AND PWD = '' OR '' = ''
```

Interpretation of WHERE condition returns TRUE because empty string is equal to empty string (' ' = ' ') and evaluation of disjunctions USERID = ' ' OR ' '= ' ' , and PWD = ' ' OR ' ' = ' ' returns TRUE and finally evaluation of conjunction USERID = ' ' OR ' ' = ' ' and PWD = ' ' OR ' ' = ' ' returns TRUE

Hence result is not empty and a variable userHasBeenAutheticated is set to True. Oh no.

**Trojans**

- A Trojan is an unauthorized program contained within a legitimate program
- A legitimate program is modified by placement of unauthorized code with it
- A legitimate program seems to do one thing but it actually does several other operations without your knowledge or agreement

A Database Trojan is an attack that consists of two phases: the injection of the malicious code and the calling of the malicious code
It is difficult to track Database Trojan because of separation in two phases, it is difficult to associate two apparently not related events
A Database Trojan after it is inserted into the system may stay in the system for a long time (“sleeper”) until it is activated

There are four categories of Trojan attacks:

- An attack that both injects a Trojan and calls it
- An attack that uses an oblivious user or process to inject a Trojan and then calls it to extract the information or perform an action within a database
- An attack that injects a Trojan and then uses an oblivious user or process to call a Trojan
- An attack that uses an oblivious user or process to inject a Trojan and also uses an oblivious user or process to call a Trojan
An example of using an oblivious user is a scenario when a junior developer gest some procedural code (e.g. trigger or stored procedure) from someone he/she does not know and then uses this code without fully understanding what it is doing.- 

**Notes on the Elimination of Vulnerabilities**

- Track processing of stored procedures
- Create baseline for a set of stored procedure
- Monitor all divergences from a baseline
- Log information and analyse the logs
- Implement a real-time alert
- Implement base-line capable firewall
- Control creation of and changes to procedures and triggers
- Watch for changes to run-as privileges
- Closely monitor developer activity on production environments
- Monitor creation of traces and event monitors
- Be aware of SQL attachments in e-mails

**Hardening MySQL environment**

- Physically secure server on which MySQL lives
- Use the following values of system variables
- local_infile = 'OFF' to disable LOCAL in LOADDATA statements
- skip_show_database= 'OFF' to ensure that show databases command only lists databases for which the user has some kind of privilege; in a more restrictive approach use skip-show-databases option
- secure_auth= 'ON' to disallow authentication for accounts that have password from earlier versions
- skip-name-resolve='ON' Do not resolve host names when checking client connections and use only IP addresses
- Do not grant PROCESS, FILE, or SUPER privileges to non-administrative users
- Do not run MySQL server on the same host as Web server in order to force remote connections
- Ensure strong password for a user root
- Disallow the default full control of the database to local users and disallow the default permissions for remote users to connect to a database
- Do not use MySQL prior to version 4.1
- Limit privileges to the load_file function
- Disallow developers to access production database servers
- Enable auditing


# Database Auditing

This involves observing a database so as to be aware of the actions of database users.

Database admins and consultants often set up auditing for security purposes.
Databse aActivity Monitor is a database securiy technology for monoitoring an analysing database activity that operates independalt yof the DBMS and does not rely on any for of native auditing or native logs such as trace or transaction logs.
DAM is typically performed in real-time.

# Auditing Categories

**Audit logon/logoff**

- record two eventsL an event for sign on and off.
- Save login name, timestamps TCP/IP address and program used to initiate connection.
- Record failed login attempts.

**Audit sources of DB usage**
 - 

**Audit database activites outside normal hours**

- Activities performed outside normal operating hours.
- Audit off hours activities including all SQL activities.
- Scheduled operations need not be audited.

**Audit DDL activites**

- DDL are the most damaging form of SQL command.
- Audiitng DDL is also done to eliminate errors of developers and database admins.
- Audit existing database schema changes by REF slides.

**Audit database errors**

Auditing database errors is important because in many cases hackers use a technique of “trial-and error” to investigate a structure of a database, a well written and tested database application does not return errors
Failed logins is a good example of errors that must be monitored
Audit database errors may also lead to identification of weak points in database applications
Audit changes to sources of stored procedures and triggers
Change to sources of already developed software may mean the attempts to incorporate malicious code
Audit can be implemented by comparison of source code developed earlier with the present one, e.g. using diff program
The second option is to use external database security and auditing system, which alerts on any modify or create command used by a database user
The third option is to use built in database features, for example ability of a system to trace “recompile” events to track changes to stored procedure and triggers

- Audit changes to privileges, user logins and other security attributes
- Audit deletion and addition of users, logins, and role
- Audit changes to the mapping between logins and users/roles
- Audit privilege changes over user and roles
- Audit password changes
- Audit changes to security attributes at a server, database, statement, or object level

**Audit creation, changes, and usage of database links and replication**

- Simple implementations using daily differences
- Comparing snapshots
- Using database internal mechanisms
- Audit changes to sensitive data (data change audit trails to enforce accuracy in financial data)
- Fully recording “old” and “new” values for each DML activity

**Due to the large number of DML operations the audits must be done very selectively**

- Carefully chose data objects to be audited
- Use database system capabilities, external audit systems, or database triggers

**Audit SELECT statements for privacy sets**

- Record where SELECT statements come from
- Who (username) retrieved the data
- What data was actually retrieved
- Identify so called privacy sets, i.e. which associations of data are really important
- Implementation uses database traces and external auditing systems

**Audit any changes made to the definitions of what to audit**
- Audit changes made to the definitions of audit trails and changes to the audit trails themselves
- Use built-in database features and external auditing systems

#Database Auditing in MySQL
Mysql has several logs that contain information about user activities

- Error log: problems encountered started with running or stopping the server.
- General query log: establised client connections and statements recieved from clients
- Binary log: Statements that change data
- Relay log: data chanes recieved from a replication master server.
- Show query log: queries that took more than a given period of time to execute.
- DDL log (metadata log)

**Error log**
Error log contains information indicating when database server was started and stopped and also any critical errors that occur while the server is running
A system variable log_error determines a location a file error.log with information included in error log
show variables like 'log_error';
+---------------+--------------------------+
| Variable_name | Value                    |
+---------------+--------------------------+
| log_error     | /var/log/mysql/error.log |
+---------------+--------------------------+

It is possible to list the contents of a file error.log in the following way:
```
cat /var/log/mysql/error.log | more
151110 14:44:15 mysqld_safe Starting mysqld daemon with
			databases from /var/lib/mysql
2015-11-10T03:44:15.916141Z 0 [Warning] Changed limits: max_open_files: 1024 
(requested 5000)
2015-11-10T03:44:15.916275Z 0 [Warning] Changed limits: table_open_cache: 431 
(requested 2000)
2015-11-10T03:44:16.831947Z 0 [Warning] 'NO_ZERO_DATE', 'NO_ZERO_IN_DATE' and
 'ERROR_FOR_DIVISION_BY_ZERO' sql modes should be used with strict mode. 
They will be merged with strict mode in a future release.
2015-11-10T03:44:16.832003Z 0 [Warning] 'NO_AUTO_CREATE_USER' sql mode was not set.
2015-11-10T03:44:16.837216Z 0 [Note] /usr/sbin/mysqld (mysqld 5.7.9) starting as 
process 22405 .
…             …            
```
**General query log**
General query log is written either to a file or to a relational table
A variable log_output determines whether general query log is written either to a file or to a relational table
```
SHOW VARIABLES LIKE 'log_output';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_output    | FILE  |
+---------------+-------+
```

A name of file for a general query log is determined by a variable general_log_file
```
SHOW VARIABLES LIKE 'general_log_file';
+------------------+---------------------------------------+
| Variable_name    | Value                                 |
+------------------+---------------------------------------+
| general_log_file | /var/lib/mysql/csit115-VirtualBox.log |
```

To redirect general query log to a relational table we change a value of variable log_output in the following way:
```
SET GLOBAL  log_output='TABLE';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_output    | TABLE |
+---------------+-------+
```

General query query log is recorded in a relational table mysql.general_log
```
DESCRIBE mysql.general_log;

+--------------+---------------------+------+-----+----------------------+--------------------------------+
| Field        | Type                | Null | Key | Default              | Extra                          |
+--------------+---------------------+------+-----+----------------------+--------------------------------+
| event_time   | timestamp(6)        | NO   |     | CURRENT_TIMESTAMP(6) | on update CURRENT_TIMESTAMP(6) |
| user_host    | mediumtext          | NO   |     | NULL                 |                                |
| thread_id    | bigint(21) unsigned | NO   |     | NULL                 |                                |
| server_id    | int(10) unsigned    | NO   |     | NULL                 |                                |
| command_type | varchar(64)         | NO   |     | NULL                 |                                |
| argument     | mediumblob          | NO   |     | NULL                 |                                |
+--------------+---------------------+------+-----+--------
```


To remove old contents of general query log execute the following TRUNCATE TABLE statement:

```
TRUNCATE TABLE mysql.general_log;
```

TRUNCATE TABLE statement is surprisingly categorized as DDL statement and it removes the contents of a relational table forever
It means that TRUNCATE TABLE statement cannot be reversed with ROLLBACK statement
To find the contents of general query log execute SELECT statement
```
SELECT * FROM mysql.general_log;

+----------------------------+---------------------------+-----------+-----------+...
| event_time                 | user_host                 | thread_id | server_id |
+----------------------------+---------------------------+-----------+-----------+...
| 2017-05-08 14:14:45.546765 | root[root] @ localhost [] |         3 |         0 | 
+----------------------------+---------------------------+-----------+-----------+...

                                                      ...--------------+---------------------------------+
                                                      ... command_type |  argument                       |
                                                      ...--------------+---------------------------------+
                                                      ... Query        | select * from mysql.general_log |
                                                      ...--------------+---------------------------------+
```
Note, that when MySQL server is shutdown and restarted all values of system variables return to its default values or values set up in the system configuration file
It means that after shutdown and restart the values of variables like general_log, log_output, general_log_file return to their original values
To stop writing into general query log execute a statement
```

SET GLOBAL general_log='OFF'
SHOW VARIABLES LIKE 'general_log';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| general_log   | OFF   |
+---------------+-------+
```

**Slow query log**

The slow query log consists of SQL statements that took more than long_query_time seconds to execute and required at least min_examined_row_limit rows to be examined
The minimum and default values of long_query_time are 0 and 10, respectively
```
SHOW VARIABLES LIKE long_query_time;
+-----------------+-----------+
| Variable_name   | Value     |
+-----------------+-----------+
| long_query_time | 10.000000 |
+-----------------+-----------+

SHOW VARIABLES LIKE min_examined_row_limit;
+------------------------+-------+
| Variable_name          | Value |
+------------------------+-------+
| min_examined_row_limit | 0     |
+------------------------+-------+
```

The value can be specified to a resolution of microseconds. For logging to a file, times are written including the microseconds part. For logging to tables, only integer times are written; the microseconds part is ignored

To change a value of long query time use the following statement
```
SET long_query_time = 0
SHOW VARIABLES LIKE 'long_query_time';
+-----------------+----------+
| Variable_name   | Value    |
+-----------------+----------+
| long_query_time | 0.000000 |
+-----------------+----------+
```


A system variable slow_query_log controls logging to slow query log
```
SHOW VARIABLES LIKE 'slow_query_log';
+----------------+-------+
| Variable_name  | Value |
+----------------+-------+
| slow_query_log | OFF   |
+----------------+-------+
```

To start logging execute the following statement
```
SET GLOBAL slow_query_log='ON'
SHOW VARIABLES LIKE 'slow_query_log';
+----------------+-------+
| Variable_name  | Value |
+----------------+-------+
| slow_query_log | ON    |
+----------------+-------+
```

Slow query log is written either to a file or to a relational table
A variable log_output determines whether general query log is written either to a file or to a relational table
```
SHOW VARIABLES LIKE 'log_output'
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_output    | FILE  |
+---------------+-------+
```
A name of file for a slow_query_log is determined by a variable slow_query_log_file
```

SHOW VARIABLES LIKE 'slow_query_log_file'
+---------------------+--------------------------------------------+
| Variable_name       | Value                                      |
+---------------------+--------------------------------------------+
| slow_query_log_file | /var/lib/mysql/csit115-VirtualBox-slow.log |
+---------------------+--------------------------------------------+
```

To redirect slow query log to a relational table we change a value of variable log_output in the following way:
```
SET GLOBAL log_output='TABLE'

+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| log_output    | TABLE |
+---------------+-------+
```

Slow query log is recorded in a relational table mysql.slow_log
```
DESCRIBE mysql.slow_log
```

To remove old contents of slow query log execute the following TRUNCATE TABLE statement
```
TRUNCATE TABLE mysql.slow_log;
```

TRUNCATE TABLE statement is surprisingly categorized as DDL statement and it removes the contents of a relational table forever
It means that TRUNCATE TABLE statement cannot be reversed with ROLLBACK statement
To find the contents of some columns from slow log execute SELECT statement

```
SELECT query_time, sql_text FROM mysql.slow_log
+-----------------+-------------------------------------------------+
| query_time      | sql_text                                        |
+-----------------+-------------------------------------------------+
| 00:00:00.034088 | truncate table mysql.slow_log                   |
| 00:00:00.000870 | select * from mysql.slow_log                    |
| 00:00:00.000914 | select query_time, sql_text from mysql.slow_log |
+-----------------+-------------------------------------------------+
```
Note, that when MySQL server is shutdown and restarted all values of system variables return to its default values or values set up in the system configuration file
It means that after shutdown and restart the values of variables like long_query_time, min_examined_row_limit, slow_query_log, log_output, slow_query_log_file return to their original values
To stop writing into slow query log execute a statement
```
SET GLOBAL slow_query_log='OFF'
SHOW VARIABLES LIKE 'slow_query_log';
```


**Binary log**
- Binary log contains “events” that describe database changes such as table creation operations or changes to table data.
- It also contains events for statements that potentially could have made changes (for example, a DELETE which matched no rows).
- Binary log also contains information about how long took the computations of UPDATE statements
- Binary log has two important purposes:
  - it is used for replication, the binary log on a master replication server provides a record of the data changes to be sent to slave servers
  - it is used for data recovery operations
- Binary log is not used for statements such as SELECT or SHOW that do not modify data
- To log all statements (for example, to identify a problem query), use general query log

**DDL log**
DDL log, or metadata log, records metadata operations generated by data definition statements such as CREATE TABLE,DROP TABLE, and ALTER TABLE
MySQL uses this log to recover from crashes occurring in the middle of a metadata operation
When executing the statement DROP TABLE t1, t2, we need to ensure that both t1 and t2 are dropped, and that each table drop is complete