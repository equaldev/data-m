/*(1) Find the names of departments located either in Japan or Singapore.*/
SELECT DEPARTMENT.department_name
FROM DEPARTMENT  
WHERE DEPARTMENT.country_name = 'Japan' OR DEPARTMENT.country_name = 'Singapore';


/*(2) Find the names of departments and names of countries located in Americas.*/
SELECT country_name, department_name
FROM DEPARTMENT JOIN COUNTRY USING (country_name) 
WHERE COUNTRY.region_name = 'Americas';

/*(3) Find the full names of employees who work in New York.*/
SELECT EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE NATURAL JOIN DEPARTMENT
WHERE city = 'New York';


/*(4) Find the full names of employees whose (commission_pct) is not empty*/
SELECT EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE
WHERE commission_pct IS NOT NULL;


/*(5) Find the job titles and total number of employees for each job title. The results will
be sorted by the job titles in ascending order*/
CREATE VIEW VTITE(  job_title, total_employees ) AS
( SELECT job_title, count(job_title)
FROM EMPLOYEE
GROUP BY job_title ASC );

SELECt * FROM VTITE;
