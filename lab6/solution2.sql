/*(1) Find the names of departments, names of countries and total number of employees
for each department that hires more than three employees.*/
CREATE VIEW V1(  department_name, country_name, total_employees_department )
	AS ( 
	SELECT DEPARTMENT.department_name, DEPARTMENT.country_name, count(EMPLOYEE.department_name)
	FROM DEPARTMENT
	LEFT OUTER JOIN EMPLOYEE 
	ON DEPARTMENT.department_name = EMPLOYEE.department_name 
	GROUP BY DEPARTMENT.department_name, DEPARTMENT.country_name
	HAVING count(EMPLOYEE.department_name) > 3
);

SELECT * FROM V1;


/*(2) Find the job titles, minimum and maximum salaries for each job title that has more
than 5 employees hired for such jobs.*/

SELECT JOB.job_title, JOB.min_salary, JOB.max_salary
FROM JOB LEFT OUTER JOIN EMPLOYEE
	on JOB.job_title = EMPLOYEE.job_title
GROUP BY JOB.job_title
HAVING count(EMPLOYEE.job_title) > 5;

/*(3) Find the employee ids, full names of employees who completed their jobs. Note:
Information about employees who have already completed their jobs is stored in a
table JOBHISTORY.*/

SELECT EMPLOYEE.employee_id, EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE JOIN JOBHISTORY
	on EMPLOYEE.employee_id = JOBHISTORY.employee_id
WHERE JOBHISTORY.end_date IS NOT NULL;

/*(4) Find the employee id, first name and last name for each employee who is directly
managed by Alberto Errazuriz.*/

SELECT EMPLOYEE.employee_id, EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE
JOIN
	(SELECT employee_id 
	FROM EMPLOYEE
	WHERE first_name = 'Alberto') A
ON EMPLOYEE.supervisor_id = A.employee_id;

/*(5) Find the employee ids, first names, last names of employees who are working on
their jobs. Note: The employees who are working on their jobs either have no record
in a table JOBHISTORY or their hire dates are later than their last jobs’ end date.*/

SELECT EMPLOYEE.employee_id, EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE
WHERE employee_id NOT IN
	(SELECT employee_id
	FROM JOBHISTORY);




