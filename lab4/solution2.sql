CREATE TABLE COMPANY (
    cname VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    street VARCHAR(50) NOT NULL,
    bldgnum VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    dname VARCHAR(50) NOT NULL,
    CONSTRAINT comp_pkey PRIMARY KEY(cname),
    CONSTRAINT comp_ckey1 UNIQUE(city,street,bldgnum),  
    CONSTRAINT comp_ckey2 UNIQUE(email),
    CONSTRAINT comp_ckey3 UNIQUE(dname)
);

CREATE TABLE PHONE (
    dname VARCHAR(50) NOT NULL,
    phoneNo VARCHAR(10) NOT NULL,
    CONSTRAINT ph_pkey PRIMARY KEY(phoneNo),
    CONSTRAINT ph_fkey FOREIGN KEY(dname)
        REFERENCES COMPANY(dname)
        ON DELETE CASCADE

);

CREATE TABLE DEPARTMENTS (
    cname VARCHAR(50) NOT NULL,
    dname VARCHAR(50) NOT NULL,
    CONSTRAINT dept_pkey PRIMARY KEY(cname),
    CONSTRAINT dept_fkey FOREIGN KEY(dname)
        REFERENCES COMPANY(dname)
        ON DELETE CASCADE
);
CREATE TABLE EMPLOYEES(
    enumber CHAR(10) NOT NULL,
    firstname VARCHAR(50) NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    dateofbirth DATE NULL,
    salary INTEGER(10) NOT NULL,
    dname VARCHAR(50) NOT NULL,
    CONSTRAINT emp_pkey PRIMARY KEY(enumber),
    CONSTRAINT emp_fkey FOREIGN KEY(dname)
        REFERENCES COMPANY(dname)
        ON DELETE CASCADE
);
