/*(1) Use a single SQL statement to create a relational table EMPCONTACT that consists of
the columns EID, FNAME, LNAME, EMAIL, and PHONENUM and such that it
contains all appropriate data copied from a relational table EMPLOYEE. When ready,
enforce appropriate consistency constraints on a relational table EMPCONTACT.*/
CREATE TABLE EMPCONTACT(
	EID    DECIMAL(6)	NOT NULL,
	FNAME  VARCHAR(20)	NOT NULL,
	LNAME  VARCHAR(25)	NOT NULL,
	EMAIL  VARCHAR(25)	    NULL,
	PHONENUM VARCHAR(20)	    NULL,
 CONSTRAINT SALESEMPLOYEE_PK PRIMARY KEY(EID),
 CONSTRAINT SALESEMPLOYEE_FK1 FOREIGN KEY(EID)
 	    REFERENCES EMPLOYEE(employee_id)
);
INSERT INTO EMPCONTACT (EID, FNAME, LNAME, EMAIL, PHONENUM)
SELECT employee_id,first_name,last_name, email, phone_number FROM EMPLOYEE;

/*(2) Create an empty relational table SALESEMPLOYEE, which has the following
columns: EID, FNAME, LNAME, and JOBTITLE with the same types as the columns
with the respective names in a relational table EMPLOYEE. Enforce primary key and
referential integrity constraints on a relational table SALESEMPLOYEE. Copy
information about all employees who have job related to Sales into
SALESEMPLOYEE. Note that if an employee has a job related with Sales then it
means that job title contains a word Sales.*/
 
CREATE TABLE SALESEMPLOYEE(
	EID    DECIMAL(6)	NOT NULL,
	FNAME  VARCHAR(20)	NULL,
	LNAME  VARCHAR(25)	NULL,
JOBTITLE       VARCHAR(35)	NULL,
 CONSTRAINT SALESEMPLOYEE_PK PRIMARY KEY(EID),
 CONSTRAINT SALESEMPLOYEE_FK1 FOREIGN KEY(EID)
 	    REFERENCES EMPLOYEE(employee_id)
);

INSERT INTO SALESEMPLOYEE (EID, FNAME, LNAME, JOBTITLE)
SELECT employee_id,first_name,last_name,job_title
FROM EMPLOYEE
WHERE job_title LIKE '%Sales%';

/*(3) Use a single UPDATE statement to increase salary by 500 for all employees that
have been hired in 1998 and earlier.*/


UPDATE EMPLOYEE SET salary = salary + 500
WHERE hire_date <= '1998/01/01';


/*(4) Add a column TOTSTAFF to a relational table DEPARTMENT. A type of the column
must be DECIMAL(3)with no other constraints imposed on the column. Next, insert
into a column TOTSTAFF the total number of employees in each department.*/
ALTER TABLE DEPARTMENT ADD COLUMN TOTSTAFF DECIMAL(3) NULL;


UPDATE DEPARTMENT, 
	(select count(EMPLOYEE.employee_id) as c
	from EMPLOYEE join DEPARTMENT
	on DEPARTMENT.department_name = EMPLOYEE.department_name 
	group by DEPARTMENT.department_name) src 
SET TOTSTAFF = src.c;


/*(5) Use a single DELETE statement to remove all data from JOBHISTORY related to the
employees who completed at least one jobs before 1996.*/ 

DELETE FROM JOBHISTORY 
WHERE employee_id IN
	(SELECT  employee_id
	 FROM (SELECT * FROM JOBHISTORY) AS A
GROUP BY employee_id
HAVING count(A.end_date) > 1 
AND JOBHISTORY.end_date  < '1996/01/01');








