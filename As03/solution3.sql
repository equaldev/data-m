/*(1) Find the full names of all employees who currently working in Canada. The query
must be implemented as a nested query.*/

SELECT EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE
WHERE EMPLOYEE.department_name IN 
	(SELECT DEPARTMENT.department_name
	 FROM DEPARTMENT
	 WHERE country_name = 'Canada'
	);

/*(2) Find the full names of all employees who have no information about themselves
recorded in JOBHISTORY table. The query must be implemented as a correlated
nested query with a negated existential quantifier.*/


SELECT EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE
WHERE EMPLOYEE.employee_id NOT IN 
	(SELECT JOBHISTORY.employee_id
	 FROM JOBHISTORY
	 WHERE JOBHISTORY.employee_id = EMPLOYEE.employee_id
	);


/*(3) Find the full names of all employees who have completed two jobs. The query must
be implemented as a nested query.*/
SELECT EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE
WHERE EMPLOYEE.employee_id IN 
	(SELECT JOBHISTORY.employee_id
	 FROM JOBHISTORY
	 GROUP BY employee_id
	 HAVING count(end_date) = 2);




/*(4) Find the names of regions together with the names of departments located in each
region. The query must be implemented as a join query.*/

SELECT DEPARTMENT.department_name, B.region_name
	FROM DEPARTMENT JOIN COUNTRY as B  
    		ON DEPARTMENT.country_name = B.country_name
		JOIN COUNTRY as A  
    		ON B.region_name = A.region_name
		GROUP BY DEPARTMENT.department_name, A.region_name;


/*(5) Find the names of departments located in Europe. The query must be
implemented as nested query with an existential quantifier.*/
SELECT DEPARTMENT.department_name
FROM DEPARTMENT
WHERE DEPARTMENT.country_name IN 
	(SELECT COUNTRY.country_name
	 FROM COUNTRY
	 WHERE region_name = 'Europe'
	);

