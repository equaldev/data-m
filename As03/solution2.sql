/*(1) Find the full names of employees who have been hired or who had their jobs in
1995.*/
SELECT EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE
WHERE employee_id IN ( SELECT JOBHISTORY.employee_id
                FROM JOBHISTORY, EMPLOYEE
                WHERE end_date < '1996/01/01' and start_date > '1995/01/01') 
OR  hire_date > '1995/01/01' and hire_date < '1996/01/01';


/*(2) Find the names of departments together with the total number of employees working
at each department. Include the departments that have no employees.*/

CREATE VIEW VNAME(  department_name, total_employees ) AS
(	SELECT DEPARTMENT.department_name, COUNT(DEPARTMENT.department_name)   
	FROM DEPARTMENT   
		INNER JOIN EMPLOYEE   
    		ON EMPLOYEE.department_name = DEPARTMENT.department_name 
	GROUP BY DEPARTMENT.department_name, EMPLOYEE.department_name
	HAVING count(EMPLOYEE.department_name) >= 0);

SELECT * FROM VNAME;


/*(3) Find the full names of supervisors together with the total number of employees
directly supervised by each one of them.*/
CREATE VIEW V1(  first_name, last_name, total_employees  ) AS
( SELECT EMPLOYEE.first_name, EMPLOYEE.last_name, count(EMPLOYEE.supervisor_id) as B
  FROM EMPLOYEE LEFT OUTER JOIN EMPLOYEE as A
                ON EMPLOYEE.employee_id = A.supervisor_id
                GROUP BY EMPLOYEE.first_name, EMPLOYEE.last_name
		HAVING count(EMPLOYEE.supervisor_id) > 1);

SELECT * FROM V1;

/*(4) Find the full names of employees who had completed their jobs and worked for more
than 1000 days.*/

SELECT EMPLOYEE.first_name, EMPLOYEE.last_name
FROM EMPLOYEE
WHERE employee_id IN ( SELECT JOBHISTORY.employee_id
                FROM JOBHISTORY
                WHERE DATE_SUB(JOBHISTORY.start_date, INTERVAL 1000 DAY) 
		AND end_date 
		IS NOT NULL);
