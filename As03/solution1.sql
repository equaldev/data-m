/*Implement the following data manipulations in SQL as the sequences of INSERT,
UPDATE, DELETE statements.

(1) A new employee James Clark has been hired at 20/04/2017 for a job of
Database analyst. He works in a department Information Technology.
The supervisor id is 104. His employee id is 207. His new email address is
james.clark@bigmail.com. His salary is 8500. The new employee does not
have commission and phone number. A new job Database analyst needs to be
added into the database. The salary of the job is between 8000 and 10000.*/
insert into JOB VALUES ('Database Analyst', '8000', '10000');

insert into EMPLOYEE VALUES ('207','James', 'Clark', 'james.clark@bigmail.com','','2017/04/20', 'Database Analyst','8500.00','0.0','104','Information Technology');





/*(2) A company decides to replace employee_id 201 with a new employee_id
208. An email of the employee with employee_id equal to 201 is MHARTSTE
and mobile phone is 515.123.5555. Update related data in the database.*/


UPDATE EMPLOYEE SET email = NULL
WHERE employee_id = '201';

UPDATE EMPLOYEE SET phone_number = NULL
WHERE employee_id = '201';

UPDATE EMPLOYEE SET supervisor_id = NULL
WHERE supervisor_id = '201';

INSERT INTO EMPLOYEE VALUES 
        ( 208
        , 'Michael'
        , 'Hartstein'
        , 'MHARTSTE'
        , '515.123.5555'
        , STR_TO_DATE('20-12-1999', '%d-%m-%Y')
        , 'Marketing Manager'
        , 13000
        , NULL
        , 100
        , 'Marketing'
);



UPDATE DEPARTMENT SET manager_id = '208'
WHERE manager_id = '201';


/*(3) An employee with employee_id equal to 102 left the company. Remove all
information about the employee from the database. Note that the employee is a
supervisor of other employees and he is not a manager of any department.*/
UPDATE EMPLOYEE SET supervisor_id = NULL
WHERE supervisor_id = '102';

DELETE FROM JOBHISTORY where employee_id = 102;
DELETE FROM EMPLOYEE where employee_id = 102;

